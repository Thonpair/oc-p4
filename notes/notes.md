# index

## a faire

* alt images : suppression paris web design


## site modifé, saisie excel en attente
* aria-label sur formulaire
* aria-required sur formulaire
* contraste orange
* modification couleur police formulaire
* label for pour les label
* supprimer lien accueil dans header (devenu inutile) + contact en haut à droite (redondant)
* url sur liens réseaux sociaux
* responsive logo réseaux sociaux sur footer
* fusion contact et index
* aria-label sur lien réseaux sociaux
* nofollow sur liens réseaux sociaux
* input comment vous nous avez connu mauvais paramètre : mail -> texte

## site modifié, saisie dans excel

* police trop petite (suppression p{11px} dans style) 
* champ saisie formulaire : message en anglais
* minifier les fichiers css et js
* cdn pour dernière version jquery 2
* suppression mots-clés en 1px autour du logo
* suppression mots-clés en bas de page, de la même couleur que le fond
* meta name="description" ne contient texte descrivant le site
* balise html, lang : default à remplacer par fr (erreur w3c) - ok
* balise title : contient un . au lieu du nom du site - ok
* images : utiliser un format d'image adapté au web et compressé - ok
* citations et title en image à transformer en texte -> citation.webp, title.webp, title2.webp à supprimer dès que mise en forme - ok
*  annuaires à supprimer - ok
* correction structure en ajoutant h2 sur la phrase avant web design / strategie / illustrations


## A resaisir
### 10 recommandations
1. Accessibilité : contraste, couleur et taille de texte
2. Accessibilité : citations images
3. Accessibilité : structure des titres
4. Accessibilité : langue html
5. Accessibilité : alt images
6. Accessibilité : Aria label sur liens réseaux sociaux
7. SEO : vitesse et taille du site : optimisation des images, compression css, js et utilisation cdn
8. SEO : Annuaire en bas de pages
9. SEO : mots clés 1px + même couleur de fond
10. SEO : balise title et meta name="description"